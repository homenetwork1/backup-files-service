#!/bin/bash


echo "load in  environment variables"

#read all the environment variables from the files in docker config path that ends with .env
export $(find $DOCKER_CONFIG_PATH -name "*.env" | xargs grep -hv '^#'| xargs -d '\n') 

echo "running application"

cron -f

