DOCKER_DEV = -f docker-compose.development.yml
DOCKER_PROD = -f docker-compose.production:w.yml

dev:
	# start develop environment
	docker-compose $(DOCKER_DEV) up ${ARGS}

dev-d:
	# start develop environment in the deamon
	ARGS=-d make dev

dev-build:
	# build docker images of the develop environment
	docker-compose $(DOCKER_DEV) build ${ARGS}

dev-build-clean:
	# build docker images of the develop environment with no cache
	ARGS=--no-cache make dev-build

dev-stop:
	# stop containers develop environment
	docker-compose $(DOCKER_DEV) stop

prod:
	# start production environment
	docker-compose $(DOCKER_PROD) up ${ARGS}

prod-d:
	# start production environment in the deamon
	ARGS=-d make prod

prod-build:
	# build docker images of the production environment
	docker-compose $(DOCKER_PROD) build ${ARGS}

prod-build-clean:
	# build docker images of the production environment with no cache
	ARGS=--no-cache make prod-build

prod-stop:
	# stop containers production environment
	docker-compose $(DOCKER_PROD) stop

backup-bash:
	#run bash inside the backup service container
	docker exec -it backup-service bash

test:
	# run the tests inside the container
	docker exec -it backup-service bash -c "pytest"

