# pull official base image
FROM python:3.8-slim-buster AS baseimage

# set environment varibles
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

ENV DOCKER_SECRET_PATH /run/secrets
ENV DOCKER_CONFIG_PATH /run/config/env

#install crontab and rsync 
RUN apt update && apt upgrade -y && \
    apt install -y cron && \
    apt install -y rsync && \
    apt install -y iputils-ping && \
    apt clean

 #set work directory
WORKDIR /usr/src/code
ENV PYTHONPATH /usr/src/code/src

#create dirs for pipenv
RUN mkdir /.local /.cache && chmod 777 /.local /.cache

 #install dependencies
RUN pip install --upgrade pip \
    && pip install pipenv
COPY ./Pipfile* /usr/src/code/
RUN pipenv install --system

#setup crontab 
RUN mkdir /var/log/cronlog && \
    touch /var/log/cronlog/cron.log && \
    touch /var/log/cronlog/rsync.log && \
    touch /var/log/cronlog/s3.log && \
    touch /var/log/cronlog/error.log
COPY src/crontab.txt /etc/cron.d/cronjob
RUN chmod 0644 /etc/cron.d/cronjob
RUN crontab /etc/cron.d/cronjob

 #copy project
COPY . /usr/src/code/


COPY scripts/entrypoint.sh /usr/local/bin/
ENTRYPOINT [ "entrypoint.sh" ]

FROM baseimage as devimage

RUN pipenv install --system --dev
