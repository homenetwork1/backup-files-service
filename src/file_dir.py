import os
import subprocess
import typing
from settings import (
    ALTERNATIVE_DIR,
)
import re


class FileDir:
    """
    class that represents a file dire local or remotely
    if remote the file does not exits it gives a local alternative
    """

    def __init__(
        self,
        path: str,
        alternative_dir: typing.Optional[str] = ALTERNATIVE_DIR
    ):
        self.__path = path
        self.__alternative_dir = alternative_dir
        self.__alternative_path: typing.Optional[str] = None
        self._username: typing.Optional[str] = None
        self._remote_server: typing.Optional[str] = None
        self._remote_path: typing.Optional[str] = None
        self._is_available: typing.Optional[bool] = None

    @property
    def path(self) -> str:
        if self._is_remote():
            self._username, self._remote_server, self._remote_path = re.split(":|@", self.__path)
            if self._server_is_available() is False:
                self._set_alternative_path()
                return self.__alternative_path

        return self.__path

    @path.setter
    def path(self, value: str):

        self.__path = value

    def _is_remote(self) -> bool:

        has_correct_char = ":" in self.__path and "@" in self.__path
        char_correct_positions = self.__path.find(":") > self.__path.find("@")
        return has_correct_char and char_correct_positions

    def _server_is_available(self,) -> bool:
        command = ['ping', '-c', '1', self._remote_server]
        self._is_available = subprocess.run(command) == 0
        return self._is_available

    def _set_alternative_path(self,) -> str:
        self.__alternative_path = str(os.path.join(
            "/", *self.__alternative_dir.split("/"), *self._remote_path.split("/")))
        return self.__alternative_path

    def log_info(self) -> str:

        if self._is_remote():
            remote_log_string = "remote path : "
            if self._is_available is None:
                remote_log_string += f"NO PING CALL: {self.__path}"
            elif self._is_available is False:
                remote_log_string += f"NOT AVAILABLE: {self.__path} -> {self.__alternative_path}"
            elif self._is_available is True:
                remote_log_string += f"AVAILABLE: {self.__path}"
            else:
                remote_log_string = "Something went wrong in the log_info"

            return remote_log_string
        else:
            return f"local path : {self.__path}"
