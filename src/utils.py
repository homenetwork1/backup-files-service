import os
import typing


class EnvironmentVariable:
    """
    returns the environment value based on the environment name
    on an instance call
    if the environment value is a path to a swarm secret
    it will return the value of that secret instead
    """

    def __init__(self,):

        self.__environment_name = ""
        self.__environment_value = None

    def __call__(self, environment_name: str, default_value: typing.Optional[str] = None) -> str:

        self.__correct_call_input_or_raise(environment_name, default_value)
        self.__environment_name = environment_name
        self.__default_value = default_value

        return self.__value()

    @staticmethod
    def __correct_call_input_or_raise(environment_name: str, default_value: typing.Optional[str]):

        name_is_string = isinstance(environment_name, str)
        if not name_is_string:
            raise Exception("The input value should be of type String")

        name_is_empty = len(environment_name) == 0
        if name_is_empty:
            raise Exception("The input value should not be empty")

        default_is_string_or_none = isinstance(default_value, str) or default_value is None
        if not default_is_string_or_none:
            raise Exception("The default value input is not of type string or none")

    def __value(self) -> str:
        env_value = self.__grab_env_value()
        if self.__is_secret(env_value):
            env_value = self.__grab_secret(env_value)

        return env_value

    @staticmethod
    def __is_secret(environment_value: typing.Optional[str]) -> bool:
        if environment_value:
            return "/var/secret" in environment_value
        else:
            return False

    def __grab_env_value(self) -> str:
        self.__environment_value = os.environ.get(self.__environment_name, self.__default_value)
        return self.__environment_value

    @staticmethod
    def __grab_secret(environment_value: str) -> str:
        f = open(environment_value, "r")
        value = f.read()
        f.close()
        return value


env = EnvironmentVariable()
