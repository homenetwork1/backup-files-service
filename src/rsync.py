import subprocess
from file_dir import FileDir
from logger import rsync_logger, rsync_error_logger
from typing import List


class RsyncCommand:

    source: FileDir = None
    destination: FileDir = None
    options = ""

    def __init__(
        self,
        source: FileDir = None,
        destination: FileDir = None,
        options: str = None,
    ):

        if source:
            self.source = source
        if destination:
            self.destination = destination
        if options:
            self.options = options

    def command_list(self) -> List[str]:
        return [
            "rsync",
            self.options,
            self.source.path,
            self.destination.path,
        ]

    def run(self):

        process = subprocess.run(self.command_list(), capture_output=True, text=True)

        output = process.stdout
        return_code = process.returncode
        error_output = process.stderr

        if return_code == 0:
            rsync_logger.info(self.log_info("SUCCES", output))
        else:
            rsync_error_logger.info(self.log_info("ERROR", error_output))

    def log_info(self, *args) -> str:

        log_info_list = [
            "RSYNC",
            ("=" * 50),
            f"command list: {self.command_list()}",
            self.log_info_paths(),
            "OUTPUT",
            ("-" * 50),
            *args,
        ]
        return "\n".join(log_info_list)

    def log_info_paths(
        self,
    ) -> str:
        source_path_log_info = f"SOURCE PATH => {self.source.log_info()}"
        destination_path_log_info = f"DESTINATION PATH => {self.destination.log_info()}"
        path_log_info_list = [
            "PATH INFO",
            ("+" * 50),
            source_path_log_info,
            destination_path_log_info,
        ]
        return "\n".join(path_log_info_list)


def factory_rsync_command(source: str, destination: str, *args, **kwargs):

    return RsyncCommand(
        source=FileDir(source), destination=FileDir(destination), *args, **kwargs
    )
