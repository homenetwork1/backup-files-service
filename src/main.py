#!/usr/local/bin/python

import json
from typing import List
from rsync import RsyncCommand, factory_rsync_command
from settings import BACKUP_CONFIG_FILE_PATH


def run_backup():
    backup_list = read_backup_config()
    for backup in backup_list:
        rsync_command: RsyncCommand = factory_rsync_command(
            source=backup["source"],
            destination=backup["destination"],
            options=backup["options"],
        )
        rsync_command.run()


def read_backup_config(file_path: str = BACKUP_CONFIG_FILE_PATH) -> List[object]:

    with open(BACKUP_CONFIG_FILE_PATH, "r") as myfile:
        data = myfile.read()

    return json.loads(data).get("data")


if __name__ == "__main__":
    run_backup()
