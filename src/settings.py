from utils import env


ALTERNATIVE_DIR = env("ALTERNATIVE_DIR", "/mnt/nextclouddrive")

RSYNC_LOG_FILE = env("RSYNC_LOG_FILE", "/var/log/cronlog/rsync.log")
RSYNC_ERROR_LOG_FILE = env("RSYNC_ERROR_LOG_FILE", "/var/log/cronlog/rsync_error.log")
BACKUP_CONFIG_FILE_PATH = env(
    "BACKUP_CONFIG_FILE_PATH", "/etc/config/backup_config.json"
)
