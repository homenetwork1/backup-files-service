import os
import pytest


from ..utils import env


@pytest.fixture(scope="class")
def environment_variable():
    os.environ["TESTING_ENV_VAR"] = "testing_environment_variable"

    yield
    os.environ.pop("TESTING_ENV_VAR")


@pytest.fixture(scope="class")
def second_environment_variable():
    os.environ["TESTING_ENV_VAR_SECOND"] = "testing_environment_variable_two"

    yield
    os.environ.pop("TESTING_ENV_VAR_SECOND")


@pytest.fixture(scope="class")
def environment_secret():
    secret_name = "/var/secret/test_secret"
    os.environ["TESTING_ENV_SECRET"] = secret_name
    if not os.path.exists("/var/secret/"):
        os.makedirs("/var/secret/")
    f = open(secret_name, "w+")
    f.write("supersecret")
    f.close()

    yield
    os.environ.pop("TESTING_ENV_SECRET")
    os.remove(secret_name)


class TestEnvironmentVariable:

    def test_wrong_input_empty_env_name(self):
        with pytest.raises(Exception) as e_info:

            env()

        assert "missing 1 required positional argument" in str(e_info.value)

    def test_wrong_input_empty_str_env_name(self):
        with pytest.raises(Exception) as e_info:

            env("")

        assert "The input value should not be empty" in str(e_info.value)

    @pytest.mark.parametrize("wrong_input", [
        ({"key": "value"},),
        (["env_name"],),
        (None,),
        (101,),
    ])
    def test_wrong_input_no_str_env_name(self, wrong_input):
        with pytest.raises(Exception) as e_info:

            env(wrong_input)

        assert "The input value should be of type String" in str(e_info.value)

    @pytest.mark.parametrize("wrong_input", [
        ({"key": "value"},),
        (["env_name"],),
        (101,),
    ])
    def test_wrong_default_input(self, wrong_input):

        with pytest.raises(Exception) as e_info:

            env("no important name", wrong_input)

        assert "The default value input is not of type string or none" in str(e_info.value)

    def test_return_correct_env_variable(self, environment_variable):
        value = env("TESTING_ENV_VAR")
        assert value == "testing_environment_variable"
        assert value == os.environ["TESTING_ENV_VAR"]

    def test_return_correct_env_secret(self, environment_secret):
        value = env("TESTING_ENV_SECRET")
        assert value is not os.environ["TESTING_ENV_SECRET"]
        assert value == "supersecret"

    def test_default_value_is_none(self, ):

        value = env("NO_ENV_VALUE", None)
        assert value is None

    def test_default_value_no_input(self, ):

        value = env("NO_ENV_VALUE")
        assert value is None

    def test_default_value_is_str(self, ):

        value = env("NO_ENV_VALUE", "default_value")
        assert value == "default_value"

    def test_multiple_env_values(self, environment_variable, second_environment_variable):
        value = env("TESTING_ENV_VAR")
        assert value == "testing_environment_variable"
        assert value == os.environ["TESTING_ENV_VAR"]

        second_value = env("TESTING_ENV_VAR_SECOND")
        assert second_value == "testing_environment_variable_two"
        assert second_value == os.environ["TESTING_ENV_VAR_SECOND"]
