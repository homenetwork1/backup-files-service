import pytest
from ..file_dir import FileDir
from ..settings import ALTERNATIVE_DIR


class TestFileDir:

    def test_simplepath(self,):
        file_dir = FileDir("./fun/path")
        assert file_dir.path == "./fun/path"

    def test_is_remote(self,):
        file_dir = FileDir("user@8.8.8.8:/fun/path")
        assert file_dir._is_remote() is True

    @pytest.mark.parametrize("path", [
        ("user@root"),
        ("@root"),
        ("user:root"),
        ("user:root@notvalid"),
    ])
    def test_is_not_remote(self, path):
        file_dir = FileDir(path)
        assert file_dir._is_remote() is False

    def test_split_remote_dir(self,):
        file_dir = FileDir("user@8.8.8.8:/fun/path")
        path = file_dir.path
        user = file_dir._username
        server = file_dir._remote_server
        remote_path = file_dir._remote_path

        assert user == "user"
        assert server == "8.8.8.8"
        assert remote_path == "/fun/path"

    def test_alternative_path(self,):

        file_dir = FileDir("user@254.8.8.8:/fun/path")
        path = file_dir.path
        assert path == ALTERNATIVE_DIR+"/fun/path"
