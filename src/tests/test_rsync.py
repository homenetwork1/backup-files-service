from ..rsync import factory_rsync_command


def test_command():
    rsync_command = factory_rsync_command(
        "/usr/src/code/watch", "/usr/src/code/backup", options="-aAXv"
    )
    rsync_command.run()
    assert True
