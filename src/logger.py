import logging

from settings import RSYNC_LOG_FILE, RSYNC_ERROR_LOG_FILE

formatter = logging.Formatter(
    "%(levelname)s: %(asctime)s : %(filename)s : %(funcName)s :%(message)s"
)
rsync_file_handler = logging.FileHandler(RSYNC_LOG_FILE)
rsync_file_handler.setFormatter(formatter)
rsync_logger = logging.getLogger("rsync")
rsync_logger.addHandler(rsync_file_handler)
rsync_logger.setLevel(logging.INFO)


rsync_error_file_handler = logging.FileHandler(RSYNC_ERROR_LOG_FILE)
rsync_error_file_handler.setFormatter(formatter)
rsync_error_logger = logging.getLogger("rsync")
rsync_error_logger.addHandler(rsync_error_file_handler)
rsync_error_logger.setLevel(logging.DEBUG)
